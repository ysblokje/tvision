/*
 * TVision example: how to change the background pattern
 *
 * Written by Sergio Sigala <sergio@sigala.it>
 * Modified by Max Okumoto <okumoto@ucsd.edu>
 */

#define Uses_TApplication
#define Uses_TDeskTop

#include <tvision/tv.h>
#include <curses.h>

class Demo: public TApplication 
{
public:
	Demo();
};

Demo::Demo(): TProgInit(&Demo::initStatusLine, &Demo::initMenuBar,
	&Demo::initDeskTop)
{
}

int main()
{
    TDeskTop::defaultBkgrnd = ACS_CKBOARD;
	Demo a;
	a.run();
}
