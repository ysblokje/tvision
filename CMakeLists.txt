cmake_minimum_required(VERSION 3.3)
project(tvision VERSION 0.8.7)

set(as_subproject tvision)

macro(find_package)
    if(NOT "${ARGV0}" IN_LIST as_subproject)
        _find_package(${ARGV})
    endif()
endmacro()


include(CheckIncludeFile)
include(FindCurses)

set(TV_VERSION "0.8.7")
set(TV_PACKAGE "tvision")
option(ENABLE_GPM  "use gpm for mouse support if available" OFF)
option(ENABLE_RUSSIAN_CHARSET  "enable russian charset" OFF)
option(DISABLE_ACS "disable alternate charset" OFF)
option(DISABLE_8BIT_CHARS "disable 8bit output" OFF)

check_include_file(gpm.h HAVE_GPM_H)
check_include_file(stdlib.h STDC_HEADERS)
check_include_file(dlfcn.h HAVE_DLFCN_H)
check_include_file(ncurses.h HAVE_NCURSES_H)

find_package(Curses)

configure_file(config.cmake.h.in config.h)


add_subdirectory(lib)

option(DISABLE_DEMO_BUILD "Disable the building of the demo application" OFF)
if(NOT DISABLE_DEMO_BUILD)
    add_subdirectory(demo)
endif()

option(DISABLE_TUT_BUILD "Disable the building of the tutorial applications" OFF)

if(NOT DISABLE_TUT_BUILD)
    add_subdirectory(tutorial)
endif()

install(DIRECTORY demo tutorial 
    DESTINATION usr/share/tvision/doc
    FILES_MATCHING PATTERN "CMakeLists.txt"
    PATTERN "*.cc"
    PATTERN "*.h"
)
    
install(FILES COPYRIGHT
    DESTINATION usr/share/tvision/doc
)
